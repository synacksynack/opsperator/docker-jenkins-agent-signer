FROM quay.io/openshift/origin-jenkins-agent-base:4.8.0

ENV COSIGN_VERSION=1.8.0

LABEL com.redhat.component="jenkins-agent-signer-centos8-container" \
      io.k8s.description="The jenkins agent signer image has the tools sining container images." \
      io.k8s.display-name="Jenkins Agent - Signer" \
      io.openshift.tags="openshift,jenkins,agent,signer" \
      architecture="x86_64" \
      name="ci/jenkins-agent-signer" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-jenkins-agent-signer" \
      version="1.0.2"

COPY config/* /

RUN set -x \
    && rm -f /etc/yum.repos.d/*.repo \
    && mv /el8.repo /etc/yum.repos.d/ \
    && mv /sign-image /usr/bin/ \
    && rpm -e --nodeps --noscripts --justdb redhat-release \
    && if dnf -y install epel-release; then \
	if test "$DO_UPGRADE"; then \
	    dnf -y upgrade; \
	fi \
	&& dnf -y install ca-certificates jq \
	&& dnf clean all -y; \
    else \
	curl https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 -o /usr/bin/jq \
	&& chmod +x /usr/bin/jq; \
    fi \
    && dnf install -y --setopt=tsflags=nodocs podman wget \
    && dnf clean all -y \
    && chown -R 1001:0 /etc/containers/registries.d \
    && chmod -R g=u /etc/containers/registries.d \
    && if uname -m | grep aarch64 >/dev/null; then \
	wget -O /usr/bin/cosign \
	    https://github.com/sigstore/cosign/releases/download/v$COSIGN_VERSION/cosign-linux-arm64; \
    else \
	wget -O /usr/bin/cosign \
	    https://github.com/sigstore/cosign/releases/download/v$COSIGN_VERSION/cosign-linux-amd64; \
    fi \
    && chmod +x /usr/bin/cosign \
    && sed -i 's/#mount_program/mount_program/' /etc/containers/storage.conf

USER 1001
