FROM docker.io/debian:buster-slim

ENV COSIGN_VERSION=1.8.0

LABEL com.redhat.component="tekton-agent-signer-buster-container" \
      io.k8s.description="The tekton agent signer image has the tools sining container images." \
      io.k8s.display-name="Tekton Agent - Signer" \
      io.openshift.tags="openshift,tekton,agent,signer" \
      name="ci/tekton-agent-signer" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-jenkins-agent-signer" \
      version="1.0.2"

COPY config/* /

RUN set -x \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	apt-get -y upgrade; \
    fi \
    && apt-get -y install curl ca-certificates jq \
    && if uname -m | grep aarch64 >/dev/null; then \
	curl -o /usr/bin/cosign -fsL \
	    https://github.com/sigstore/cosign/releases/download/v$COSIGN_VERSION/cosign-linux-arm64; \
    elif uname -m | grep armv7l; then \
	curl -o /usr/bin/cosign -fsL \
	    https://github.com/sigstore/cosign/releases/download/v$COSIGN_VERSION/cosign-linux-arm; \
    else \
	curl -o /usr/bin/cosign -fsL \
	    https://github.com/sigstore/cosign/releases/download/v$COSIGN_VERSION/cosign-linux-amd64; \
    fi \
    && chmod +x /usr/bin/cosign \
    && apt-get remove -y curl \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
