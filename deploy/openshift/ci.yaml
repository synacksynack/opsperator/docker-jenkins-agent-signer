apiVersion: v1
kind: Template
labels:
  app: jenkins-agent-signer
  template: jenkins-agent-signer-jenkins-pipeline
metadata:
  annotations:
    description: Jenkins Agent Signer Image - Jenkinsfile
      see https://gitlab.com/synacksynack/opsperator/docker-jenkins-agent-signer
    iconClass: icon-openshift
    openshift.io/display-name: Jenkins Agent Signer CI
    tags: jenkins-agent-signer
  name: jenkins-agent-signer-jenkins-pipeline
objects:
- apiVersion: v1
  kind: BuildConfig
  metadata:
    annotations:
      description: Builds Jenkins Agent Signer images
    name: jenkinsagentsigner-jenkins-pipeline
  spec:
    strategy:
      jenkinsPipelineStrategy:
        jenkinsfile: |-
          def gitCommitMsg = ''
          def templateMark = 'jas-jenkins-ci'
          def templateSel  = 'jenkins-ci-mark'
          pipeline {
              agent {
                  node { label 'maven' }
              }
              options { timeout(time: 130, unit: 'MINUTES') }
              parameters {
                  string(defaultValue: 'master', description: 'Jenkins Agent Signer Docker Image - Source Git Branch', name: 'jenkinsagentsignerBranch')
                  string(defaultValue: 'master', description: 'Jenkins Agent Signer Docker Image - Source Git Hash', name: 'jenkinsagentsignerHash')
                  string(defaultValue: '${GIT_SOURCE_HOST}/faust64/docker-jenkins-agent-signer.git', description: 'Jenkins Agent Signer Docker Image - Source Git Repository', name: 'jenkinsagentsignerRepo')
                  string(defaultValue: '3', description: 'Max Retry', name: 'jobMaxRetry')
                  string(defaultValue: '1', description: 'Retry Count', name: 'jobRetryCount')
              }
              stages {
                  stage('pre-cleanup') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      echo "Using project: ${openshift.project()}"
                                      echo "cleaning up previous assets for jas-${params.jenkinsagentsignerHash}"
                                      openshift.selector("all", [ "${templateSel}": "${templateMark}-${params.jenkinsagentsignerHash}" ]).delete()
                                      openshift.selector("secrets", [ "${templateSel}": "${templateMark}-${params.jenkinsagentsignerHash}" ]).delete()
                                  }
                              }
                          }
                      }
                  }
                  stage('create') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      def namespace = "${openshift.project()}"
                                      try {
                                          timeout(10) {
                                              def cloneProto = "http"
                                              def created
                                              def objectsFromTemplate
                                              def privateRepo = false
                                              def repoHost = params.jenkinsagentsignerRepo.split('/')[0]
                                              def templatePath = "/tmp/workspace/${namespace}/${namespace}-jenkinsagentsigner-jenkins-pipeline/tmpjas${params.jenkinsagentsignerBranch}/openshift"
                                              sh "git config --global http.sslVerify false"
                                              sh "rm -fr tmpjas${params.jenkinsagentsignerBranch}; mkdir -p tmpjas${params.jenkinsagentsignerBranch}"
                                              dir ("tmpjas${params.jenkinsagentsignerBranch}") {
                                                  try {
                                                      withCredentials([string(credentialsId: "git-${repoHost}", variable: 'GIT_TOKEN')]) {
                                                          cloneProto = "https"
                                                          privateRepo = true
                                                          echo "cloning ${params.jenkinsagentsignerRepo} over https, using ${repoHost} token"
                                                          try { git([ branch: "${params.jenkinsagentsignerBranch}", url: "https://${GIT_TOKEN}@${params.jenkinsagentsignerRepo}" ]) }
                                                          catch(e2) {
                                                              echo "Failed cloning ${params.jenkinsagentsignerRepo}#${params.jenkinsagentsignerBranch} - ${e2}"
                                                              throw e2
                                                          }
                                                      }
                                                  } catch(e) {
                                                      if (privateRepo != true) {
                                                          echo "caught ${e} - assuming no credentials required"
                                                          echo "cloning ${params.jenkinsagentsignerRepo} over http"
                                                          try { git([ branch: "${params.jenkinsagentsignerBranch}", url: "http://${params.jenkinsagentsignerRepo}" ]) }
                                                          catch(e2) {
                                                              echo "Failed cloning ${params.jenkinsagentsignerRepo}#${params.jenkinsagentsignerBranch} - ${e2}"
                                                              throw e2
                                                          }
                                                      } else { throw e }
                                                  }
                                                  try {
                                                      gitCommitMsg = sh(returnStdout: true, script: "git log -n 1").trim()
                                                  } catch(e) { echo "In non-critical catch block resolving commit message - ${e}" }
                                              }
                                              try { sh "test -d ${templatePath}" }
                                              catch (e) {
                                                  echo "Could not find ./openshift in ${params.jenkinsagentsignerRepo}#${params.jenkinsagentsignerBranch}"
                                                  throw e
                                              }
                                              echo "Processing JenkinsAgentSigner:${params.jenkinsagentsignerHash}, from ${repoHost}, tagging to ${params.jenkinsagentsignerBranch}"
                                              try {
                                                  echo " == Creating ImageStream =="
                                                  objectsFromTemplate = openshift.process("-f", "${templatePath}/imagestream.yaml")
                                                  echo "The template will create ${objectsFromTemplate.size()} objects"
                                                  created = openshift.apply(objectsFromTemplate)
                                                  created.withEach { echo "Created ${it.name()} with labels ${it.object().metadata.labels}" }
                                              } catch(e) { echo "In non-critical catch block while creating ImageStream - ${e}" }
                                              echo " == Creating BuildConfigs =="
                                              if (privateRepo) {
                                                  withCredentials([string(credentialsId: "git-${repoHost}", variable: 'GIT_TOKEN')]) {
                                                      objectsFromTemplate = openshift.process("-f", "${templatePath}/build-with-secret.yaml", '-p', "GIT_DEPLOYMENT_TOKEN=${GIT_TOKEN}",
                                                          '-p', "JAS_REPOSITORY_REF=${params.jenkinsagentsignerHash}", '-p', "JAS_REPOSITORY_URL=${cloneProto}://${params.jenkinsagentsignerRepo}")
                                                  }
                                              } else {
                                                  objectsFromTemplate = openshift.process("-f", "${templatePath}/build.yaml",
                                                      '-p', "JAS_REPOSITORY_REF=${params.jenkinsagentsignerHash}", '-p', "JAS_REPOSITORY_URL=${cloneProto}://${params.jenkinsagentsignerRepo}")
                                              }
                                              echo "The template will create ${objectsFromTemplate.size()} objects"
                                              for (o in objectsFromTemplate) { o.metadata.labels["${templateSel}"] = "${templateMark}-${params.jenkinsagentsignerHash}" }
                                              created = openshift.apply(objectsFromTemplate)
                                              created.withEach { echo "Created ${it.name()} from template with labels ${it.object().metadata.labels}" }
                                          }
                                      } catch(e) {
                                          echo "In catch block while creating resources - ${e}"
                                          throw e
                                      }
                                  }
                              }
                          }
                      }
                  }
                  stage('build') {
                      steps {
                          script {
                              openshift.withCluster() {
                                  openshift.withProject() {
                                      try {
                                          timeout(90) {
                                              echo "watching jas-${params.jenkinsagentsignerHash} docker image build"
                                              def builds = openshift.selector("bc", [ name: "jas-${params.jenkinsagentsignerHash}" ]).related('builds')
                                              builds.untilEach(1) { return (it.object().status.phase == "Complete") }
                                          }
                                      } catch(e) {
                                          echo "In catch block while building Docker image - ${e}"
                                          throw e
                                      }
                                  }
                              }
                          }
                      }
                  }
                  stage('tag') {
                      steps {
                          script {
                              if ("${params.jenkinsagentsignerBranch}" == "${params.jenkinsagentsignerHash}") { echo "skipping tag - source matches target" }
                              else {
                                  openshift.withCluster() {
                                      openshift.withProject() {
                                          try {
                                              timeout(5) {
                                                  def namespace = "${openshift.project()}"
                                                  retry(3) {
                                                      sh """
                                                      oc login https://kubernetes.default.svc.cluster.local --certificate-authority=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt --token=\$(cat /var/run/secrets/kubernetes.io/serviceaccount/token) > /dev/null 2>&1
                                                      oc describe -n ${namespace} imagestreamtag jenkins-agent-signer:${params.jenkinsagentsignerHash} || exit 1
                                                      oc tag -n ${namespace} jenkins-agent-signer:${params.jenkinsagentsignerHash} jenkins-agent-signer:${params.jenkinsagentsignerBranch}
                                                      """
                                                  }
                                              }
                                          } catch(e) {
                                              echo "In catch block while tagging Jenkins Agent Signer image - ${e}"
                                              throw e
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
              post {
                  always {
                      script {
                          openshift.withCluster() {
                              openshift.withProject() {
                                  def namespace   = "${openshift.project()}"
                                  def postJobName = "${namespace}/${namespace}-post-triggers-jenkins-pipeline"
                                  currentBuild.description = """
                                  ${params.jenkinsagentsignerRepo} ${params.jenkinsagentsignerBranch} (try ${params.jobRetryCount}/${params.jobMaxRetry})
                                  ${gitCommitMsg}
                                  """.stripIndent()
                                  echo "cleaning up assets for jas-${params.jenkinsagentsignerHash}"
                                  sh "rm -fr /tmp/workspace/${namespace}/${namespace}-jenkinsagentsigner-jenkins-pipeline/tmpjas${params.jenkinsagentsignerBranch}"
                                  openshift.selector("all", [ "${templateSel}": "${templateMark}-${params.jenkinsagentsignerHash}" ]).delete()
                                  openshift.selector("secrets", [ "${templateSel}": "${templateMark}-${params.jenkinsagentsignerHash}" ]).delete()
                                  def jobParams = [
                                          [$class: 'StringParameterValue', name: "jobMaxRetry", value: params.jobMaxRetry],
                                          [$class: 'StringParameterValue', name: "jobRetryCount", value: params.jobRetryCount],
                                          [$class: 'StringParameterValue', name: "jobStatus", value: currentBuild.currentResult],
                                          [$class: 'StringParameterValue', name: "sourceBranch", value: params.jenkinsagentsignerBranch],
                                          [$class: 'StringParameterValue', name: "sourceComponent", value: "jenkinsagentsigner"],
                                          [$class: 'StringParameterValue', name: "sourceImageStream", value: "jenkins-agent-signer"],
                                          [$class: 'StringParameterValue', name: "sourceRef", value: params.jenkinsagentsignerHash],
                                          [$class: 'StringParameterValue', name: "sourceRepo", value: params.jenkinsagentsignerRepo]
                                      ]
                                  try { build job: postJobName, parameters: jobParams, propagate: false, wait: false }
                                  catch(e) { echo "caught ${e} starting Job post-process" }
                              }
                          }
                      }
                  }
                  changed { echo "changed?" }
                  failure { echo "Build failed (${params.jobRetryCount} out of ${params.jobMaxRetry})" }
                  success { echo "success!" }
                  unstable { echo "unstable?" }
              }
          }
      type: JenkinsPipeline
parameters:
- name: GIT_SOURCE_HOST
  description: Git FQDN we would build images from
  displayName: Git
  value: gitlab.com
